<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="<?php echo base_url('assets/frontend/img'); ?>/logo_small.png" />
  <link rel="stylesheet" href="<?php echo base_url('assets/frontend'); ?>/css/bootstrap.min.css">

  <script src="<?php echo base_url('assets/frontend'); ?>/js/jquery.min.js"></script>
  <script src="<?php echo base_url('assets/frontend'); ?>/js/bootstrap.min.js"></script>

  <link href="<?php echo base_url('assets/frontend'); ?>/plugin/aos/aos.css" rel="stylesheet">
  <link href="<?php echo base_url('assets/frontend'); ?>/plugin/web-fonts-with-css/css/fontawesome-all.css" rel="stylesheet">
  
</script>

<script>
    $(function () {
      $(document).scroll(function () {
        var $nav = $(".navbar-fixed-top");
        $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
      });
    });
  </script>
  
<style>
  /* Remove the navbar's default margin-bottom and rounded borders */ 
  /* Add a gray background color and some padding to the footer */
  footer {
    position:absolute;
    margin-top:70px;
    width: 100%;
    padding: 25px;
    background: #800000;
    color: #fff;
    text-align: center;
  }
  body{
    background: #e8e8e8;
  }
  .navbar {
      background-color: transparent;
      background:rgba(256,256,256,0.4);
      border: none;
      margin-bottom: 0;
      border-radius: 0;
   }
 .navbar li a, .navbar {
  color: #fff !important;
}
.navbar-nav li a:hover, .navbar-nav li.active a {
  color: #fff !important;
  background-color: #800000 !important;
}
.navbar-default .navbar-toggle {
  border-color: transparent;
  color: #fff !important;
}
@media (min-width: 768px) {
  .navbar-nav.navbar-center {
    position: absolute;
    left: 50%;
    transform: translatex(-50%);
  }
}
.navbar-fixed-top.scrolled {
    background-color: rgba(0,0,0,1); !important;
    transition: background-color 200ms linear;
  }
.img-logo{
    width: 275px;
    margin-top:5px;
  }
.sosmed{
  padding-right: 100px;
  padding-left: 100px;
  padding-bottom: 10px
}
.judul{
  padding-top: 70px;
  padding-bottom:30px;
  color: #800000;
}
.map{
  text-align: center
}
.form-control::-webkit-input-placeholder { color: #800000; }  /* WebKit, Blink, Edge */
.form-control:-moz-placeholder { color: #800000; }  /* Mozilla Firefox 4 to 18 */
.form-control::-moz-placeholder { color: #800000; }  /* Mozilla Firefox 19+ */
.form-control:-ms-input-placeholder { color: #800000; }  /* Internet Explorer 10-11 */
.form-control::-ms-input-placeholder { color: #800000; }  /* Microsoft Edge */

input[type=text],input[type=email],input[type=subject] {
  width: 100%;
  border: none;
  background-color: #cccccc;
  color: #800000;
}
  .danger{
    background: #e6e6e6;
    color: #800000;
    border-color: #800000
  }
  .danger:hover {
   background: #800000;
   color: #fff;
 }

 @font-face {
    font-family: Font1;
    src: url(<?php echo base_url('assets/frontend'); ?>/fonts/Roadgeek2005Series2W.woff);
  }

  .navbar-brand,.navbar,h1,h2,h3,h4,h5,h6,p,.h1,.h2,.h3,.h4,.h5,.h6,button
  {
    font-family: 'Font1';
    font-variant: inherit;

  }

</style>
</head>
<body>

  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/frontend') ?>/img/solusi_consulting.png" class="img-logo"></a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-center" style="text-shadow: 2px 2px 2px #4d4d4d">
          <li><a href="<?php echo base_url('consultation'); ?>">CONSULTATION</a></li>
          <li><a href="<?php echo base_url('training'); ?>">TRAINING</a></li>
          <li><a href="<?php echo base_url('gallery'); ?>">GALLERY</a></li>
          <li><a href="<?php echo base_url('aboutus'); ?>">ABOUT US</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right" style="text-shadow: 2px 2px 2px #4d4d4d">
          <li class="active"><a href="<?php echo base_url('contactus'); ?>" style="border-bottom: 3px solid #800000;">CONTACT US</a></li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 judul" style="text-align: center;text-shadow: 2px 2px 1px #b3b3b3">
        <h1><i><b>SOLUSI CONSULTING</b></i></h1>  
      </div>
    </div>

    <div class="col-sm-12 sosmed" style="text-align: justify;color:#800000">
      <div class="col-sm-4">
        <p><span class="fab fa-whatsapp fa-3x fa-pull-left"></span>
          PHONE / WHATSAPP <span style="color: #000">(ALVIN SUSIADI)</span><br>
          <span style="color: #000">0811 821 709</span></p>
        </div>
        <div class="col-sm-4">
          <span class="far fa-envelope fa-3x fa-pull-left"></span>
          <p>E-MAIL<br>
            <span style="color: #000">alvin.susiadi@gmail.com</span></p>
          </div>
          <div class="col-sm-4" style="text-align: left">
            <span class="fas fa-map-marker-alt fa-3x fa-pull-left"></span>
            <p>ADDRESS<br>
              <span style="color: #000">Citra Gran, Blok G5 No. 11, Cibubur, Jakarta 17435. </span></p>
            </div>

          </div>
          </div>
          <div class="container-fluid">
          <div class="row">
          <div class="col-sm-6">
            <div class="col-sm-12">
                <?php if($this->session->flashdata('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php endif; ?>
              <form action="<?php echo base_url('contactus/send'); ?>" method="post" enctype="multipart/form-data">
              <div class="row">
                <div class="col-sm-6 form-group">
                  <input name="name" class="form-control" id="name" required="" type="text" placeholder="Nama" require>
                </div>
                <div class="col-sm-6 form-group">
                  <input name="email" class="form-control" id="email" required="" type="email" placeholder="Email" require>
                </div>
                <div class="col-sm-12 form-group">
                  <input name="subject" class="form-control" id="subject" required="" type="subject" placeholder="Subjek" require>
                </div>
              </div>
              <textarea name="message" class="form-control" id="message" placeholder="Pesan" rows="5" style="background: #cccccc" require></textarea><br>
              <div class="row" style="text-align: center;">
                <div class="col-sm-12 form-group">
                  <button class="btn btn-md danger" type="submit">KIRIM PESAN</button>
                </div>
              </div>
              </form>
            </div>
          </div>

          <div class="col-sm-6">
           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.0950637238525!2d106.92246131464714!3d-6.381729995382261!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6994a6198564f3%3A0xff4d924991401d75!2sCitra+Gran+Cibubur!5e0!3m2!1sen!2sid!4v1531385125004" width="580" height="205" frameborder="0" style="border:0" allowfullscreen></iframe> 
         </div>
         </div>
         </div>

     </body>
     <footer>
      <p>© Solusi Consulting. All rights reserved.</p>
    </footer>
    <script src="<?php echo base_url('assets/frontend'); ?>/plugin/aos/aos.js"></script>
    <script type="text/javascript">
      AOS.init({
        startEvent: 'load',
      });
      AOS.refresh();
    </script>
    </html>