<!DOCTYPE html>
<html lang="en">
<head>
  <title>Homepage</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="<?php echo base_url('assets/frontend/img'); ?>/logo_small.png" />
  <link rel="stylesheet" href="<?php echo base_url('assets/frontend'); ?>/css/bootstrap.min.css">

  <script src="<?php echo base_url('assets/frontend'); ?>/js/jquery.min.js"></script>
  <script src="<?php echo base_url('assets/frontend'); ?>/js/bootstrap.min.js"></script>

  <link href="<?php echo base_url('assets/frontend'); ?>/plugin/aos/aos.css" rel="stylesheet">
  <link href="<?php echo base_url('assets/frontend'); ?>/plugin/web-fonts-with-css/css/fontawesome-all.css" rel="stylesheet">
  
  <script>
    $(function () {
      $(document).scroll(function () {
        var $nav = $(".navbar-fixed-top");
        $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
      });
    });
  </script>

  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      background-color: transparent;
      background:rgba(256,256,256,0.4);
      border: none;
      margin-bottom: 0;
      border-radius: 0;
   }
   .navbar li a, .navbar {
    color: #fff !important;
  }
  .navbar-nav li a:hover, .navbar-nav li.active a {
    color: #fff !important;
    background-color: #800000 !important;
  }
  .navbar-default .navbar-toggle {
    border-color: transparent;
    color: #fff !important;
  }
  @media (min-width: 768px) {
    .navbar-nav.navbar-center {
      position: absolute;
      left: 50%;
      transform: translatex(-50%);
    }
  }
  .navbar-fixed-top.scrolled {
    background-color: rgba(0,0,0,1); !important;
    transition: background-color 200ms linear;
  }
  .img-logo{
    width: 275px;
    margin-top:5px;
  }

  footer {
    background: #800000;
    color: #fff;
    text-align: center;
  }
  body{
    background: #e6e6e6
  }

  .form-control::-webkit-input-placeholder { color: #800000; }  /* WebKit, Blink, Edge */
  .form-control:-moz-placeholder { color: #800000; }  /* Mozilla Firefox 4 to 18 */
  .form-control::-moz-placeholder { color: #800000; }  /* Mozilla Firefox 19+ */
  .form-control:-ms-input-placeholder { color: #800000; }  /* Internet Explorer 10-11 */
  .form-control::-ms-input-placeholder { color: #800000; }  /* Microsoft Edge */

  @font-face {
    font-family: Font1;
    src: url(<?php echo base_url('assets/frontend'); ?>/fonts/Roadgeek2005Series2W.woff);
  }
  .navbar-brand,.navbar,h1,h2,h3,h4,h5,h6,p,.h1,.h2,.h3,.h4,.h5,.h6,button
  {
    font-family: 'Font1';
    font-variant: inherit;

  }
  .danger{
    background: #e6e6e6;
    color: #800000;
    border: solid 3px #800000;
	border-radius: 0;
  }
  .danger:hover {
   background: #800000;
   color: #fff;
   border: none;
 }
 .danger1{
  background: #800000;
  color: #fff;
  border-color: #800000;
  border-radius: 0;
}
.danger1:hover {
 background: #e6e6e6;
 color: #800000;
 border-color:#fff;
}

.parallax-head{
  background-image: url(<?php echo base_url('assets/frontend'); ?>/img/2.jpg);
  min-height: 700px;
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  padding-top: 150px;
}
.bucen {
  display: flex; 
  justify-content: 
  center;
}
.sepasi{
  width:20px;
  height:auto;
  display:inline-block;
}

.circle ol {
  counter-reset:item; 
  margin:0; 
  padding-left:0; 
}
.circle ol>li {
  counter-increment:item; 
  list-style:none inside; 
  margin: 10px 0;
  overflow: hidden;
  font-size: 16px !important;
  line-height: 2;
}
.circle ol>li:before {
  content:counter(item) ;
  margin-right: 15px;
  margin-left: 30px;
  padding: 8px;
  display: block;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  width: 35px;
  background: #800000 ;
  color: #fff;
  text-align: center; 
  font: 15px 'Lato', Helvetica, Arial, sans-serif;
  font-weight: 100;
  float: left;
}

.circle1 ol {
  counter-reset:item; 
  margin:0; 
  padding-left:0; 
}
.circle1 ol>li {
  counter-increment:item; 
  list-style:none inside; 
  margin: 10px 0;
  overflow: hidden;
  font-size: 16px !important;
  line-height: 2;
}
.circle1 ol>li:before {
  content:counter(item) ;
  margin-right: 15px;
  margin-left: 30px;
  padding: 8px;
  display: block;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  width: 35px;
  background: #e6e6e6 ;
  color: #800000;
  text-align: center; 
  font: 15px 'Lato', Helvetica, Arial, sans-serif;
  font-weight: 100;
  float: left;
}

.jumbotron {
  background-image: linear-gradient(rgba(255,255,255,0.5), rgba(255,255,255,0.5)), url("<?php echo base_url('assets/frontend'); ?>/img/3.jpg");
  margin-bottom: 0;
  min-height: 50%;
  background-repeat: no-repeat;
  background-position: center;
  -webkit-background-size: cover;
  background-size: cover;
  background-attachment: fixed;
  min-height: 500px;
  padding-top: 100px
}

.carousel-control {
  width:4%;
  height:0;
  top:50%;
}    

.carousel-inner > .item > img, .carousel-inner > .item > a > img {
  width: 100%;
}
.carousel {
  position: relative;
  padding: 10px;
}

.carousel-control.left {
  margin-left: -45px;
}

.carousel-control.right {
  margin-right: -45px;
}

.merah{
  background-image: linear-gradient(rgba(128, 0, 0, 0.5), rgba(128, 0, 0, 0.5)), url("img/3.jpg");
  margin-bottom: 0;
  min-height: 50%;
  background-repeat: no-repeat;
  background-position: center;
  -webkit-background-size: cover;
  background-size: cover;
  background-attachment: fixed;
  min-height: 150px;
  padding-top: 50px;
  padding-bottom: 50px;
  width: 100%;
}
.col-half-offset{
  margin-left:3%
}

.img-client{
  margin-top: 50px;
    height : 150px;
    width: 200px;
}
.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 200px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a:hover {
    background-color: #ddd;
}

.dropdown:hover .dropdown-content {
    display: block;
}
</style>
</head>
<body>

  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/frontend') ?>/img/solusi_consulting.png" class="img-logo"></a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-center" style="text-shadow: 2px 2px 2px #4d4d4d">
          <li><a href="<?php echo base_url('consultation'); ?>">CONSULTATION</a></li>
          <li class="dropdown">
            <a href="<?php echo base_url('training'); ?>">TRAINING</a>
          </li>
          <li><a href="<?php echo base_url('gallery'); ?>">GALLERY</a></li>
          <li><a href="<?php echo base_url('aboutus'); ?>">ABOUT US</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right" style="text-shadow: 2px 2px 2px #4d4d4d;">
          <li class="active"><a href="<?php echo base_url('contactus'); ?>" style="border-bottom: 3px solid #800000;">CONTACT US</a></li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="parallax-head">
    <strong>
      <h1 align="center" style="color: #800000; font-size: 5em;"><i>SOLUSI CONSULTING</i></h1>
      <p align="center" style="color:#fff;font-size: 1.5em;">WE PROVIDE SOLUTION. WE DELIVER IMPROVEMENT.</p>
      <p align="center" style="color:#fff;font-size: 1.5em;">WE AIM FOR YOUR SUCCESS</p>
    </strong>

    <div class="bucen">
      <a href="<?php echo base_url('consultation'); ?>" class="btn btn-default btn-lg danger1" type="submit" style="">Daftar Konsultasi</a>
      <div class="sepasi"></div>
      <a href="<?php echo base_url('training'); ?>" class="btn btn-default btn-lg danger" type="submit" style="">Daftar Training</a>
    </div>
  </div>

  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-6" style="padding:0">
        <img src="<?php echo base_url('assets/frontend'); ?>/img/555.jpg" style="width: 100%;max-height: 350px" data-aos="flip-right">
      </div>
      <div class="col-sm-6 circle">
        <h1 style="padding-left: 2em;color:#800000;text-shadow: 2px 2px 1px #b3b3b3;"><b>CONSULTATION</b></h1>
        <ol>
          <?php foreach($consult->result() as $c): ?>
            <li><?php echo $c->sertifikat; ?></li>
          <?php endforeach; ?>
        </ol>
        <a href="<?php echo base_url('consultation'); ?>" class="btn btn-md danger1 pull-right" style="box-shadow: 0 4px 8px 0 #4d4d4d; margin-bottom:10px;">Daftar Konsultasi</a>
      </div>
    </div>
  </div>

  <div class="container-fluid" style="background: #800000;color: #fff">
    <div class="row">
      <div class="col-sm-6 circle1">
        <h1 style="padding-left: 2em;color:#e6e6e6;text-shadow: 2px 2px 1px #1a1a1a;"><b>TRAINING</b></h1>
        <ol>
        <?php foreach($training->result() as $t): ?>
            <li><?php echo $t->sertifikat; ?></li>
        <?php endforeach; ?>
        </ol> 
        <a href="<?php echo base_url('training'); ?>" class="btn btn-md danger pull-right" style="box-shadow: 0 4px 8px 0 #1a1a1a; margin-bottom:10px;">Daftar Training</a>
      </div>
      <div class="col-sm-6" style="padding:0">
        <img src="<?php echo base_url('assets/frontend'); ?>/img/beside_training.jpg" style="width: 100%;max-height: 350px" data-aos="flip-left">
      </div>
    </div>
  </div>

  <div class="container-fluid" style="padding-top: 5em">
    <div class="row">
      <div class="col-sm-6 circle" style="padding: 0">
        <h1 style="color:#800000"><b><center>STRENGTH</center></b></h1>
        <ol style="padding-bottom: 1em">
          <li>Berpengalaman</li>
          <li>Menggunakan metode yang efektif</li>
          <li>Merancang konsultasi dan training sesuai dengan permasalahan klien</li>
        </ol>
      </div>
      <div class="col-sm-6 circle" style="padding: 0">
        <h1 style="color:#800000"><b><center>ADVANTAGES</center></b></h1>
        <ol style="padding-bottom: 1em">
          <li>Meningkatkan kompetensi usaha</li>
          <li>Mendapatkan sertifikat standar dunia</li>
          <li>Menghemat biaya usaha</li>
        </ol>
      </div>
    </div>
  </div>
  
  <div class="col-sm-6" style="padding: 0">
	<img src="<?php echo base_url('assets/frontend'); ?>/img/strength.jpg" style="width: 100%" class="img" data-aos="flip-right">
  </div>
  <div class="col-sm-6" style="padding: 0">
	<img src="<?php echo base_url('assets/frontend'); ?>/img/advantages.jpg" style="width: 100%" class="img" data-aos="flip-right">
  </div>

  <div class="jumbotron" style="padding-bottom: 200px;margin-top: 250px">
    <h2 style="color: #800000"><center><b>CLIENTS</b></center></h2><br>
    <div class="container-fluid merah">
      <div class="col-md-12">

        <div id="myCarousel" class="carousel slide">
          <!-- Carousel items -->
          <div class="carousel-inner" >
            <div class="item active">
              <div class="row" style="padding-top: 30px">
                <div class="col-md-2 col-sm-12 col-xs-12 col-half-offset"><a href=""><img src="<?php echo base_url('uploads'); ?>/ANUGRAH.png" alt="Image" class="img-responsive img-client"></a>
                </div>
                <div class="col-md-2 col-sm-12 col-xs-12 col-half-offset"><a href=""><img src="<?php echo base_url('uploads'); ?>/ASAHI.png" alt="Image" class="img-responsive img-client"></a>
                </div>
                <div class="col-md-2 col-sm-12 col-xs-12 col-half-offset"><a href=""><img src="<?php echo base_url('uploads'); ?>/BUMIMULIA.png" alt="Image" class="img-responsive img-client"></a>
                </div>
                <div class="col-md-2 col-sm-12 col-xs-12 col-half-offset"><a href=""><img src="<?php echo base_url('uploads'); ?>/B4T.png" alt="Image" class="img-responsive img-client"></a>
                </div>
                <div class="col-md-2 col-sm-12 col-xs-12 col-half-offset"><a href=""><img src="<?php echo base_url('uploads'); ?>/BUMI_SIAK.png" alt="Image" class="img-responsive img-client"></a>
                </div>
              <div class="row" style="padding-top: 30px">
              </div>
                <div class="col-md-2 col-sm-12 col-xs-12 col-half-offset"><a href=""><img src="<?php echo base_url('uploads'); ?>/DANRILIS.png" alt="Image" class="img-responsive img-client"></a>
                </div>
                <div class="col-md-2 col-sm-12 col-xs-12 col-half-offset"><a href=""><img src="<?php echo base_url('uploads'); ?>/DUTAPALMA.png" alt="Image" class="img-responsive img-client"></a>
                </div>
                <div class="col-md-2 col-sm-12 col-xs-12 col-half-offset"><a href=""><img src="<?php echo base_url('uploads'); ?>/IAIN_AMBON.png" alt="Image" class="img-responsive img-client"></a>
                </div>
                <div class="col-md-2 col-sm-12 col-xs-12 col-half-offset"><a href=""><img src="<?php echo base_url('uploads'); ?>/INTI_STANIA.png" alt="Image" class="img-responsive img-client"></a>
                </div>
                <div class="col-md-2 col-sm-12 col-xs-12 col-half-offset"><a href=""><img src="<?php echo base_url('uploads'); ?>/KAHATEX.png" alt="Image" class="img-responsive img-client"></a>
                </div>
              </div>
              <!--/row-->
            </div>
            <!--/item-->
            <div class="item">
              <div class="row">
                <div class="col-md-2 col-sm-12 col-xs-12 col-half-offset"><a href=""><img src="<?php echo base_url('uploads'); ?>/KEMIKO.png" alt="Image" class="img-responsive img-client"></a>
                </div>
                <div class="col-md-2 col-sm-12 col-xs-12 col-half-offset"><a href=""><img src="<?php echo base_url('uploads'); ?>/KINO.png" alt="Image" class="img-responsive img-client"></a>
                </div>
                <div class="col-md-2 col-sm-12 col-xs-12 col-half-offset"><a href=""><img src="<?php echo base_url('uploads'); ?>/LOTTE_PACKAGING.png" alt="Image" class="img-responsive img-client"></a>
                </div>
                <div class="col-md-2 col-sm-12 col-xs-12 col-half-offset"><a href=""><img src="<?php echo base_url('uploads'); ?>/LPMP.png" alt="Image" class="img-responsive img-client"></a>
                </div>
                <div class="col-md-2 col-sm-12 col-xs-12 col-half-offset"><a href=""><img src="<?php echo base_url('uploads'); ?>/MANDOM.png" alt="Image" class="img-responsive img-client"></a>
                </div>
              <div class="row" style="padding-top: 30px">
              </div>
                <div class="col-md-2 col-sm-12 col-xs-12 col-half-offset"><a href=""><img src="<?php echo base_url('uploads'); ?>/MITSUBISHI_LOGISTICS.png" alt="Image" class="img-responsive img-client"></a>
                </div>
                <div class="col-md-2 col-sm-12 col-xs-12 col-half-offset"><a href=""></a>
                </div>
                <div class="col-md-2 col-sm-12 col-xs-12 col-half-offset"><a href=""></a>
                </div>
                <div class="col-md-2 col-sm-12 col-xs-12 col-half-offset"><a href=""></a>
                </div>
                <div class="col-md-2 col-sm-12 col-xs-12 col-half-offset"><a href=""></a>
                </div>
              </div>
              <!--/row-->
            </div>
            
          </div>
          <!--/carousel-inner--> 
          <a class="left carousel-control" href="#myCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
          <a class="right carousel-control" href="#myCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>

        </div>
        <!--/myCarousel-->
        <!--/well-->
      </div>
    </div>
  </div>

</body>
<!-- mulai footernya -->
<footer class="container-fluid">
  <div class="row" style="background: #1a1a1a; color:#fff">
    <div class="col-sm-12">
      <h2 class="" style="text-shadow: 2px 2px 1px #4d4d4d; padding-bottom: 10px"><b>CONTACT US</b></h2>
      <!-- mulai bagian sosmednya sebelah kiri -->
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="col-md-5" style="text-align: justify;">
            <p><span class="fab fa-whatsapp" style="font-size: 1.5em; color: #fff"></span><b style="color:#b30000; margin-left: 0.5em"> PHONE / WHATSAPP</b> (ALVIN SUSIADI)</p>
            <p style="margin-left: 2em ;line-height:0.1;margin-bottom: 1em"> 0811 821 709</p>
            <p><span class="far fa-envelope" style="font-size: 1.5em; color: #fff"></span><b style="color:#b30000; margin-left: 0.5em"> E-MAIL</b></p>
            <p style="margin-left: 2em;line-height:0.1;margin-bottom: 1em"> alvin.susiadi@gmail.com</p>
            <p><span class="fas fa-map-marker-alt" style="font-size: 1.5em; color: #fff"></span><b style="color:#b30000; margin-left: 0.5em"> ADDRESS</b></p>
            <p style="margin-left: 2em;line-height:0.1;margin-bottom: 1em"> itra Gran, Blok G5 No. 11, Cibubur, Jakarta 17435.</p>
          </div>
          <!-- akhir bagian sosmed sebelah kiri -->

          <!-- mulai form kontak -->
          <form action="<?php echo base_url('contactus/send'); ?>" method="post" enctype="multipart/form-data">
          <div class="col-sm-7">
            <div class="row">
              <div class="col-sm-6 form-group">
                <input name="name" class="form-control" id="name" required="" type="text" placeholder="Nama" require>
              </div>
              <div class="col-sm-6 form-group">
                <input name="email" class="form-control" id="email" required="" type="email" placeholder="Email" require>
              </div>
              <div class="col-sm-12 form-group">
                <input name="subject" class="form-control" id="subject" required="" type="subject" placeholder="Subjek" require>
              </div>
            </div>
            <textarea name="pesan" class="form-control" id="pesan" placeholder="Pesan" rows="5" require></textarea><br>
            <div class="row" style="text-align: center;">
              <div class="col-sm-12 form-group">
                <button class="btn btn-md danger" type="submit">KIRIM PESAN</button>
              </div>
            </div>
          </div>
          </form>
          <!-- akhir form kontak -->
        </div>
      </div>
    </div>

    <!-- mulai copyrightnya -->
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12" style="background: #800000;color: #fff">
          <div class="col-sm-12">
            <p style="text-align: center; padding-top: 10px;padding-bottom: 10px">
              © Solusi Consulting. All rights reserved.
            </p>
          </div>
        </div>
      </div>
    </div>
    <!-- akhir copyright -->

  </div>
</footer>


<script src="<?php echo base_url('assets/frontend'); ?>/plugin/aos/aos.js"></script>
<script type="text/javascript">
  AOS.init({
    startEvent: 'load',
  });
  AOS.refresh();
</script>
</html>
