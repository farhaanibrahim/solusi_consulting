<!DOCTYPE html>
<html lang="en">
<head>
  <title>About Us</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo base_url('assets/frontend'); ?>/css/bootstrap.min.css">
  <link rel="shortcut icon" href="<?php echo base_url('assets/frontend/img'); ?>/logo_small.png" />

  <script src="<?php echo base_url('assets/frontend'); ?>/js/jquery.min.js"></script>
  <script src="<?php echo base_url('assets/frontend'); ?>/js/bootstrap.min.js"></script>

  <link href="<?php echo base_url('assets/frontend'); ?>/plugin/aos/aos.css" rel="stylesheet">
  <link href="<?php echo base_url('assets/frontend'); ?>/plugin/web-fonts-with-css/css/fontawesome-all.css" rel="stylesheet">
  
  <script>
    $(function () {
      $(document).scroll(function () {
        var $nav = $(".navbar-fixed-top");
        $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
      });
    });
  </script>
  
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      background-color: transparent;
      background:rgba(256,256,256,0.4);
      border: none;
      margin-bottom: 0;
      border-radius: 0;
   }
   .navbar li a, .navbar {
    color: #fff !important;
  }
  .navbar-nav li a:hover, .navbar-nav li.active a {
    color: #fff !important;
    background-color: #800000 !important;
  }
  .navbar-default .navbar-toggle {
    border-color: transparent;
    color: #fff !important;
  }
  @media (min-width: 768px) {
    .navbar-nav.navbar-center {
      position: absolute;
      left: 50%;
      transform: translatex(-50%);
    }
  }
  .navbar-fixed-top.scrolled {
    background-color: rgba(0,0,0,1); !important;
    transition: background-color 200ms linear;
  }
  .img-logo{
    width: 275px;
    margin-top:5px;
  }

  footer {
    background: #800000;
    color: #fff;
    text-align: center;
  }
  body{
    background: #e6e6e6
  }

  .form-control::-webkit-input-placeholder { color: #800000; }  /* WebKit, Blink, Edge */
  .form-control:-moz-placeholder { color: #800000; }  /* Mozilla Firefox 4 to 18 */
  .form-control::-moz-placeholder { color: #800000; }  /* Mozilla Firefox 19+ */
  .form-control:-ms-input-placeholder { color: #800000; }  /* Internet Explorer 10-11 */
  .form-control::-ms-input-placeholder { color: #800000; }  /* Microsoft Edge */

  @font-face {
    font-family: Font1;
    src: url(<?php echo base_url('assets/frontend'); ?>/fonts/Roadgeek2005Series2W.woff);
  }
  .navbar-brand,.navbar,h1,h2,h3,h4,h5,h6,p,.h1,.h2,.h3,.h4,.h5,.h6,button
  {
    font-family: 'Font1';
    font-variant: inherit;

  }
  .danger{
    background: #e6e6e6;
    color: #800000;
    border-color: #800000
  }
  .danger:hover {
   background: #800000;
   color: #fff;
   border-color: #fff;
 }

 .parallax-head{
  background-image: url(<?php echo base_url('assets/frontend'); ?>/img/header_gallery.jpg);
  min-height: 650px;
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  padding-top: 250px;
  padding-bottom: 10px;
}
.ukuran{
  height:350px
}
img{
  object-fit: cover;
}
.text {
  color: #fff;
  font-size: 35px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
  text-shadow: 2px 2px 4px #4d4d4d;
  visibility: hidden;
  transition: 1s;
  opacity : 0;
}
.img_wrap{
	background: #800000;
}

.img_wrap img{
	opacity: 1;
  transition: opacity .25s ease-in-out;
  -moz-transition: opacity .25s ease-in-out;
  -webkit-transition: opacity .25s ease-in-out;
}
.img_wrap:hover .text{
  visibility: visible;
  opacity: 1;
}

.img_wrap:hover img{
  opacity: 0.6;
}


/**
 * Material Modal CSS
 */
.modal {
  will-change: visibility, opacity;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  overflow-y: auto;
  overflow-x: hidden;
  z-index: 1000;
  visibility: hidden;
  opacity: 0;
  -webkit-transition: all 0.5s cubic-bezier(0.23, 1, 0.32, 1);
  transition: all 0.5s cubic-bezier(0.23, 1, 0.32, 1);
  -webkit-transition-delay: $modal-delay;
          transition-delay: $modal-delay;
}
.modal--active {
  visibility: visible;
  opacity: 1;
}
.modal--align-top {
  -webkit-box-align: start;
      -ms-flex-align: start;
          align-items: flex-start;
}
.modal__bg {
  background: transparent;
}
.modal__dialog {
  max-width: 600px;
  padding: 1.2rem;
}
.modal__content {
  will-change: transform, opacity;
  position: relative;
  padding: 2.4rem;
  background: #fff;
  background-clip: padding-box;
  box-shadow: 0 12px 15px 0 rgba(0,0,0,0.25);
  opacity: 0;
  -webkit-transition: all 0.25s cubic-bezier(0.23, 1, 0.32, 1);
  transition: all 0.25s cubic-bezier(0.23, 1, 0.32, 1);
}
.modal__content--active {
  opacity: 1;
}
.modal__close {
  z-index: 1100;
  cursor: pointer;
}
#modal__temp {
  will-change: transform, opacity;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: transparent;
  -webkit-transform: none;
          transform: none;
  opacity: 1;
  -webkit-transition: opacity 0.1s ease-out, -webkit-transform 0.5s cubic-bezier(0.23, 1, 0.32, 1);
  transition: opacity 0.1s ease-out, -webkit-transform 0.5s cubic-bezier(0.23, 1, 0.32, 1);
  transition: opacity 0.1s ease-out, transform 0.5s cubic-bezier(0.23, 1, 0.32, 1);
  transition: opacity 0.1s ease-out, transform 0.5s cubic-bezier(0.23, 1, 0.32, 1), -webkit-transform 0.5s cubic-bezier(0.23, 1, 0.32, 1);
}
img {
  max-width: 100%;
}
.demo-btns header {
  padding: 7vh 10vw;
  background: #ffebee;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
}
.demo-btns header h1 {
  margin: 0;
  color: rgba(0,0,0,0.54);
  font-weight: 300;
}
.demo-btns .info {
  background: #f44336;
  padding: 3vh 10vw;
  height: 70vh;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  -ms-flex-flow: column wrap;
      flex-flow: column wrap;
}
.demo-btns p {
  text-align: center;
  color: #fff;
}
.demo-btns .link {
  font-size: 20px;
}
.demo-close {
  position: absolute;
  top: 0;
  right: 0;
  margin: 1.2rem;
  padding: 0.6rem;
  background: rgba(0,0,0,0.3);
  border-radius: 50%;
  -webkit-transition: all 0.5s cubic-bezier(0.23, 1, 0.32, 1);
  transition: all 0.5s cubic-bezier(0.23, 1, 0.32, 1);
}
.demo-close svg {
  width: 24px;
  fill: #fff;
  pointer-events: none;
  vertical-align: top;
}
.demo-close:hover {
  background: rgba(0,0,0,0.6);
}
.logo {
  position: fixed;
  bottom: 3vh;
  right: 3vw;
  z-index: 2;
}
.logo img {
  width: 45px;
  -webkit-transform: rotate(0);
          transform: rotate(0);
  -webkit-transition: all 0.5s cubic-bezier(0.23, 1, 0.32, 1);
  transition: all 0.5s cubic-bezier(0.23, 1, 0.32, 1);
}
.logo img:hover {
  -webkit-transform: rotate(180deg) scale(1.1);
          transform: rotate(180deg) scale(1.1);
}

</style>
</head>
<body>

  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/frontend') ?>/img/solusi_consulting.png" class="img-logo"></a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-center" style="text-shadow: 2px 2px 2px #4d4d4d">
          <li><a href="<?php echo base_url('consultation'); ?>">CONSULTATION</a></li>
          <li><a href="<?php echo base_url('training'); ?>">TRAINING</a></li>
          <li style="border-bottom: 3px solid #800000;"><a href="<?php echo base_url('gallery'); ?>">GALLERY</a></li>
          <li><a href="<?php echo base_url('aboutus'); ?>">ABOUT US</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right" style="text-shadow: 2px 2px 2px #4d4d4d;">
          <li class="active"><a href="<?php echo base_url('contactus'); ?>" style="border-bottom: 3px solid #800000;">CONTACT US</a></li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="parallax-head">
    <h1 align="center" style="color: #800000; font-size: 6em;text-shadow: 2px 2px 4px #4d4d4d">GALLERY</h1>
  </div>

  <div class="container-fluid">
    <div class="row">
      <?php foreach ($img->result() as $row) {
        ?>
        <div class="col-xs-4 img_wrap" style="padding:0">
          <img src="<?php echo base_url('uploads'); ?>/<?php echo $row->foto; ?>" class="ukuran img-responsive" style="width: 100%" data-aos="flip-right">
          <a href="" data-modal="#<?php echo $row->id; ?>" class="modal__trigger"><h1 class="text"><b><?php echo $row->judul; ?></b></h1></a>
        </div>
        
        <!-- Modal -->
        <div id="<?php echo $row->id; ?>" class="modal modal__bg" role="dialog" aria-hidden="true">
        <div class="modal__dialog">
          <div class="modal__content">
            <h1><?php echo $row->judul; ?></h1>
            <p><?php echo $row->deskripsi; ?></p>
            
            <!-- modal close button -->
            <a href="" class="modal__close demo-close">
              <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
            </a>
            
          </div>
        </div>
      </div>
        <?php
      } ?>
    </div>
  </div>

</body>
<!-- mulai footernya -->
<footer class="container-fluid">
  <div class="row" style="background: #1a1a1a; color:#fff">
    <div class="col-sm-12">
      <h2 class="" style="text-shadow: 2px 2px 1px #4d4d4d; padding-bottom: 10px"><b>CONTACT US</b></h2>
      <!-- mulai bagian sosmednya sebelah kiri -->
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="col-md-5" style="text-align: justify;">
            <p><span class="fab fa-whatsapp" style="font-size: 1.5em; color: #fff"></span><b style="color:#b30000; margin-left: 0.5em"> PHONE / WHATSAPP</b> (ALVIN SUSIADI)</p>
            <p style="margin-left: 2em ;line-height:0.1;margin-bottom: 1em"> 0811 821 709</p>
            <p><span class="far fa-envelope" style="font-size: 1.5em; color: #fff"></span><b style="color:#b30000; margin-left: 0.5em"> E-MAIL</b></p>
            <p style="margin-left: 2em;line-height:0.1;margin-bottom: 1em"> alvin.susiadi@gmail.com</p>
            <p><span class="fas fa-map-marker-alt" style="font-size: 1.5em; color: #fff"></span><b style="color:#b30000; margin-left: 0.5em"> ADDRESS</b></p>
            <p style="margin-left: 2em;line-height:0.1;margin-bottom: 1em"> itra Gran, Blok G5 No. 11, Cibubur, Jakarta 17435.</p>
          </div>
          <!-- akhir bagian sosmed sebelah kiri -->

          <!-- mulai form kontak -->
          <form action="<?php echo base_url('contactus/send'); ?>" method="post" enctype="multipart/form-data">
          <div class="col-sm-7">
            <div class="row">
              <div class="col-sm-6 form-group">
                <input name="name" class="form-control" id="name" required="" type="text" placeholder="Nama" require>
              </div>
              <div class="col-sm-6 form-group">
                <input name="email" class="form-control" id="email" required="" type="email" placeholder="Email" require>
              </div>
              <div class="col-sm-12 form-group">
                <input name="subject" class="form-control" id="subject" required="" type="subject" placeholder="Subjek" require>
              </div>
            </div>
            <textarea name="pesan" class="form-control" id="pesan" placeholder="Pesan" rows="5" require></textarea><br>
            <div class="row" style="text-align: center;">
              <div class="col-sm-12 form-group">
                <button class="btn btn-md danger" type="submit">KIRIM PESAN</button>
              </div>
            </div>
          </div>
          </form>
          <!-- akhir form kontak -->
        </div>
      </div>
    </div>

    <!-- mulai copyrightnya -->
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12" style="background: #800000;color: #fff">
          <div class="col-sm-12">
            <p style="text-align: center; padding-top: 10px;padding-bottom: 10px">
              © Solusi Consulting. All rights reserved.
            </p>
          </div>
        </div>
      </div>
    </div>
    <!-- akhir copyright -->

  </div>
</footer>


<script src="<?php echo base_url('assets/frontend'); ?>/plugin/aos/aos.js"></script>
<script type="text/javascript">
  AOS.init({
    startEvent: 'load',
  });
  AOS.refresh();
</script>
<script>
	    var Modal = (function() {

  var trigger = $qsa('.modal__trigger'); // what you click to activate the modal
  var modals = $qsa('.modal'); // the entire modal (takes up entire window)
  var modalsbg = $qsa('.modal__bg'); // the entire modal (takes up entire window)
  var content = $qsa('.modal__content'); // the inner content of the modal
	var closers = $qsa('.modal__close'); // an element used to close the modal
  var w = window;
  var isOpen = false;
	var contentDelay = 400; // duration after you click the button and wait for the content to show
  var len = trigger.length;

  // make it easier for yourself by not having to type as much to select an element
  function $qsa(el) {
    return document.querySelectorAll(el);
  }

  var getId = function(event) {

    event.preventDefault();
    var self = this;
    // get the value of the data-modal attribute from the button
    var modalId = self.dataset.modal;
    var len = modalId.length;
    // remove the '#' from the string
    var modalIdTrimmed = modalId.substring(1, len);
    // select the modal we want to activate
    var modal = document.getElementById(modalIdTrimmed);
    // execute function that creates the temporary expanding div
    makeDiv(self, modal);
  };

  var makeDiv = function(self, modal) {

    var fakediv = document.getElementById('modal__temp');

    /**
     * if there isn't a 'fakediv', create one and append it to the button that was
     * clicked. after that execute the function 'moveTrig' which handles the animations.
     */

    if (fakediv === null) {
      var div = document.createElement('div');
      div.id = 'modal__temp';
      self.appendChild(div);
      moveTrig(self, modal, div);
    }
  };

  var moveTrig = function(trig, modal, div) {
    var trigProps = trig.getBoundingClientRect();
    var m = modal;
    var mProps = m.querySelector('.modal__content').getBoundingClientRect();
    var transX, transY, scaleX, scaleY;
    var xc = w.innerWidth / 2;
    var yc = w.innerHeight / 2;

    // this class increases z-index value so the button goes overtop the other buttons
    trig.classList.add('modal__trigger--active');

    // these values are used for scale the temporary div to the same size as the modal
    scaleX = mProps.width / trigProps.width;
    scaleY = mProps.height / trigProps.height;

    scaleX = scaleX.toFixed(3); // round to 3 decimal places
    scaleY = scaleY.toFixed(3);


    // these values are used to move the button to the center of the window
    transX = Math.round(xc - trigProps.left - trigProps.width / 2);
    transY = Math.round(yc - trigProps.top - trigProps.height / 2);

		// if the modal is aligned to the top then move the button to the center-y of the modal instead of the window
    if (m.classList.contains('modal--align-top')) {
      transY = Math.round(mProps.height / 2 + mProps.top - trigProps.top - trigProps.height / 2);
    }


		// translate button to center of screen
		trig.style.transform = 'translate(' + transX + 'px, ' + transY + 'px)';
		trig.style.webkitTransform = 'translate(' + transX + 'px, ' + transY + 'px)';
		// expand temporary div to the same size as the modal
		div.style.transform = 'scale(' + scaleX + ',' + scaleY + ')';
		div.style.webkitTransform = 'scale(' + scaleX + ',' + scaleY + ')';


		window.setTimeout(function() {
			window.requestAnimationFrame(function() {
				open(m, div);
			});
		}, contentDelay);

  };

  var open = function(m, div) {

    if (!isOpen) {
      // select the content inside the modal
      var content = m.querySelector('.modal__content');
      // reveal the modal
      m.classList.add('modal--active');
      // reveal the modal content
      content.classList.add('modal__content--active');

      /**
       * when the modal content is finished transitioning, fadeout the temporary
       * expanding div so when the window resizes it isn't visible ( it doesn't
       * move with the window).
       */

      content.addEventListener('transitionend', hideDiv, false);

      isOpen = true;
    }

    function hideDiv() {
      // fadeout div so that it can't be seen when the window is resized
      div.style.opacity = '0';
      content.removeEventListener('transitionend', hideDiv, false);
    }
  };

  var close = function(event) {

		event.preventDefault();
    event.stopImmediatePropagation();

    var target = event.target;
    var div = document.getElementById('modal__temp');

    /**
     * make sure the modal__bg or modal__close was clicked, we don't want to be able to click
     * inside the modal and have it close.
     */

    if (isOpen && target.classList.contains('modal__bg') || target.classList.contains('modal__close')) {

      // make the hidden div visible again and remove the transforms so it scales back to its original size
      div.style.opacity = '1';
      div.removeAttribute('style');

			/**
			* iterate through the modals and modal contents and triggers to remove their active classes.
      * remove the inline css from the trigger to move it back into its original position.
			*/

			for (var i = 0; i < len; i++) {
				modals[i].classList.remove('modal--active');
				content[i].classList.remove('modal__content--active');
				trigger[i].style.transform = 'none';
        trigger[i].style.webkitTransform = 'none';
				trigger[i].classList.remove('modal__trigger--active');
			}

      // when the temporary div is opacity:1 again, we want to remove it from the dom
			div.addEventListener('transitionend', removeDiv, false);

      isOpen = false;

    }

    function removeDiv() {
      setTimeout(function() {
        window.requestAnimationFrame(function() {
          // remove the temp div from the dom with a slight delay so the animation looks good
          div.remove();
        });
      }, contentDelay - 50);
    }

  };

  var bindActions = function() {
    for (var i = 0; i < len; i++) {
      trigger[i].addEventListener('click', getId, false);
      closers[i].addEventListener('click', close, false);
      modalsbg[i].addEventListener('click', close, false);
    }
  };

  var init = function() {
    bindActions();
  };

  return {
    init: init
  };

}());

Modal.init();
	</script>
</html>
