<!DOCTYPE html>
<html lang="en">
<head>
  <title>404 Page Not Found</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://localhost/solusi_consulting/assets/frontend/css/bootstrap.min.css" type="text/css">

  <script src="http://localhost/solusi_consulting/assets/frontend/js/jquery.min.js"></script>
  <script src="http://localhost/solusi_consulting/assets/frontend/js/bootstrap.min.js"></script>

  <link href="http://localhost/solusi_consulting/assets/frontend/plugin/aos/aos.css" rel="stylesheet" type="text/css">
  <link href="http://localhost/solusi_consulting/assets/frontend/plugin/web-fonts-with-css/css/fontawesome-all.css" rel="stylesheet" type="text/css">
  
  <script>
    $(function () {
      $(document).scroll(function () {
        var $nav = $(".navbar-fixed-top");
        $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
      });
    });
  </script>

  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
     background:#cccccc;
     border: none;
     margin-bottom: 0;
     border-radius: 0;
   }
   .navbar li a, .navbar {
    color: #fff !important;
  }
  .navbar-nav li a:hover, .navbar-nav li.active a {
    color: #fff !important;
    background-color: #800000 !important;
  }
  .navbar-default .navbar-toggle {
    border-color: transparent;
    color: #fff !important;
  }
  @media (min-width: 768px) {
    .navbar-nav.navbar-center {
      position: absolute;
      left: 50%;
      transform: translatex(-50%);
    }
  }
  .navbar-fixed-top.scrolled {
    background-color: rgba(0,0,0,1); !important;
    transition: background-color 200ms linear;
  }
  .img-logo{
    width: 275px;
    margin-top:5px;
  }

  /* Set black background color, white text and some padding */
  footer {
    background: #800000;
    color: #fff;
    text-align: center;
  }
  body{
    background: #e6e6e6
  }
  .image-size{
    width: 800px;
    height: 400px;
  }
  .card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 40%;
    border-radius: 5px;
  }
  .form-control::-webkit-input-placeholder { color: #800000; }  /* WebKit, Blink, Edge */
  .form-control:-moz-placeholder { color: #800000; }  /* Mozilla Firefox 4 to 18 */
  .form-control::-moz-placeholder { color: #800000; }  /* Mozilla Firefox 19+ */
  .form-control:-ms-input-placeholder { color: #800000; }  /* Internet Explorer 10-11 */
  .form-control::-ms-input-placeholder { color: #800000; }  /* Microsoft Edge */

  .gap-bottom{
    margin-bottom: 5px;
  }

  hr {
    display: block;
    height: 1px;
    border: 0;
    border-top: 1px solid #800000;
    margin: 1em 0;
    padding: 0; 
  }
  @font-face {
    font-family: Font1;
    src: url(<?php echo base_url('assets/frontend'); ?>/fonts/Roadgeek2005Series2W.woff);
  }
  .navbar-brand,.navbar,h1,h2,h3,h4,h5,h6,p,.h1,.h2,.h3,.h4,.h5,.h6,button
  {
    font-family: 'Font1';
    font-variant: inherit;

  }
  .dangeru{
    background: #e6e6e6;
    color: #800000;
    border-color: #800000
  }
  .dangeru:hover {
   background: #800000;
   color: #fff;
 }
</style>
</head>
<body>

  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/frontend') ?>/img/solusi_consulting.png" class="img-logo"></a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-center" style="text-shadow: 2px 2px 2px #4d4d4d">
          <li><a href="/solusi_consulting/consultation">CONSULTATION</a></li>
          <li><a href="/solusi_consulting/training">TRAINING</a></li>
          <li><a href="/solusi_consulting/gallery">GALLERY</a></li>
          <li><a href="/solusi_consulting/aboutus">ABOUT US</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right" style="text-shadow: 2px 2px 2px #4d4d4d">
          <li class="active"><a href="/solusi_consulting/contactus">CONTACT US</a></li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="jumbotron text-center" style="padding:130px;color:#800000">
    <h1><b>OOPS! <br>
      404 Page Not Found</b></h1> 
      <p><b>Sorry, an error has occured,Requestes page not found!</b></p> 
      <form class="form-inline">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Cari..">
          <div class="input-group-btn">
            <button type="button" class="btn btn-md dangeru">Cari</button>
          </div>
        </div>
      </form>
    </div> 

  </body>
  <!-- mulai footernya -->
  <footer class="container-fluid">
    <div class="row" style="background: #1a1a1a; color:#fff">
      <div class="col-sm-12">
        <h2 class="" style="text-shadow: 2px 2px 1px #4d4d4d; padding-bottom: 10px"><b>CONTACT US</b></h2>
        <!-- mulai bagian sosmednya sebelah kiri -->
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <div class="col-md-5" style="text-align: justify;">
              <p><span class="fab fa-whatsapp" style="font-size: 1.5em; color: #fff"></span><b style="color:#b30000; margin-left: 0.5em"> PHONE / WHATSAPP</b> (ALVIN SUSIADI)</p>
              <p style="margin-left: 2em ;line-height:0.1;margin-bottom: 1em"> 0811 821 709 / 0878 8588 5838</p>
              <p><span class="far fa-envelope" style="font-size: 1.5em; color: #fff"></span><b style="color:#b30000; margin-left: 0.5em"> E-MAIL</b></p>
              <p style="margin-left: 2em;line-height:0.1;margin-bottom: 1em"> ALVIN.SUSIADI@GMAIL.COM</p>
              <p><span class="fas fa-map-marker-alt" style="font-size: 1.5em; color: #fff"></span><b style="color:#b30000; margin-left: 0.5em"> ADDRESS</b></p>
              <p style="margin-left: 2em;line-height:0.1;margin-bottom: 1em"> JL.DUREN TIGA UTARA NO.54.JAKARTA SELATAN</p>
            </div>
            <!-- akhir bagian sosmed sebelah kiri -->

            <!-- mulai form kontak -->
            <form action="<?php echo base_url('contactus/send'); ?>" method="post" enctype="multipart/form-data">
            <div class="col-sm-7">
              <div class="row">
                <div class="col-sm-6 form-group">
                  <input name="name" class="form-control" id="name" required="" type="text" placeholder="Nama">
                </div>
                <div class="col-sm-6 form-group">
                  <input name="email" class="form-control" id="email" required="" type="email" placeholder="Email">
                </div>
                <div class="col-sm-12 form-group">
                  <input name="subject" class="form-control" id="subject" required="" type="subject" placeholder="Subjek">
                </div>
              </div>
              <textarea name="pesan" class="form-control" id="pesan" placeholder="Pesan" rows="5"></textarea><br>
              <div class="row" style="text-align: center;">
                <div class="col-sm-12 form-group">
                  <button class="btn btn-md dangeru" type="submit">KIRIM PESAN</button>
                </div>
              </div>
            </div>
            </form>
            <!-- akhir form kontak -->
          </div>
        </div>
      </div>

      <!-- mulai copyrightnya -->
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-12" style="background: #800000;color: #fff">
            <div class="col-sm-12">
              <p style="text-align: center; padding-top: 10px;padding-bottom: 10px">
                © Solusi Consulting. All rights reserved.
              </p>
            </div>
          </div>
        </div>
      </div>
      <!-- akhir copyright -->

    </div>
  </footer>


  <script src="http://localhost/solusi_consulting/assets/frontend/plugin/aos/aos.js"></script>
  <script type="text/javascript">
    AOS.init({
      startEvent: 'load',
    });
    AOS.refresh();
  </script>
  </html>
