<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Training extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('DataModel');
    }
    public function index()
    {
        $data['training'] = $this->DataModel->getTraining();
        $this->load->view('frontend/training',$data);
    }

    public function in_house()
    {
        $data['training'] = $this->DataModel->getTrainingByID(1);
        $data['category'] = 'In - house';
        $this->load->view('frontend/category_training',$data);
    }

    public function public_training()
    {
        $data['training'] = $this->DataModel->getTrainingByID(2);
        $data['category'] = 'Public';
        $this->load->view('frontend/category_training',$data);
    }
}
