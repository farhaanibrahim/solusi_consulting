<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('DataModel');
	}

	public function index()
	{
		$data['consult'] = $this->DataModel->getConsultingForHomepage();
		$data['training'] = $this->DataModel->getTrainingForHomepage();
		$data['client'] = $this->DataModel->getClient();
		$this->load->view('frontend/homepage',$data);
	}
}
