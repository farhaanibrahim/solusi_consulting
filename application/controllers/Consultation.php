<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Consultation extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('DataModel');
    }
    public function index()
    {
        $data['consult'] = $this->DataModel->getConsulting();
        $this->load->view('frontend/consultation',$data);
    }

    public function quality_management()
    {
        $id_consult = 1;
        $data['category'] = 'Quality Management';
        $data['consult'] = $this->DataModel->getConsultingByID($id_consult);

        $this->load->view('frontend/category_consultation',$data);
    }

    public function environment_energy()
    {
        $id_consult = 2;
        $data['category'] = 'Environment Energy';
        $data['consult'] = $this->DataModel->getConsultingByID($id_consult);

        $this->load->view('frontend/category_consultation',$data);
    }

    public function health_safety()
    {
        $id_consult = 3;
        $data['category'] = 'Health Safety';
        $data['consult'] = $this->DataModel->getConsultingByID($id_consult);

        $this->load->view('frontend/category_consultation',$data);
    }

    public function food_safety()
    {
        $id_consult = 4;
        $data['category'] = 'Food Safety';
        $data['consult'] = $this->DataModel->getConsultingByID($id_consult);

        $this->load->view('frontend/category_consultation',$data);
    }

    public function information_security()
    {
        $id_consult = 5;
        $data['category'] = 'Information Security';
        $data['consult'] = $this->DataModel->getConsultingByID($id_consult);

        $this->load->view('frontend/category_consultation',$data);
    }

    public function risk_management()
    {
        $id_consult = 6;
        $data['category'] = 'Risk Management';
        $data['consult'] = $this->DataModel->getConsultingByID($id_consult);

        $this->load->view('frontend/category_consultation',$data);
    }

    public function other()
    {
        $id_consult = 7;
        $data['category'] = 'Other Consultation';
        $data['consult'] = $this->DataModel->getConsultingByID($id_consult);

        $this->load->view('frontend/category_consultation',$data);
    }
}
