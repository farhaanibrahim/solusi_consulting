<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('DataModel');
    }
    public function index()
    {
        $data['img'] = $this->DataModel->getGallery();
        $this->load->view('frontend/gallery',$data);
    }
}
