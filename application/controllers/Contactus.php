<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contactus extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('DataModel');
    }
    public function index()
    {
        $this->load->view('frontend/contactus');
    }

    public function send()
    {
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $subjek = $this->input->post('subject');
        $message = "Message From : ".$name."<br>Email : ".$email."<br>Subjek : ".$subjek."<br>Message : <br>".$this->input->post('message')."";

        $data = array(
            'nama'=>$name,
            'email'=>$email,
            'subjek'=>$subjek,
            'pesan'=>$message,
            'tgl'=>date('Ymd'),
            'status'=>'unread'
        );

        $this->DataModel->sendMessage($data);

        //Load email library
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.gmail.com';
        $config['smtp_port'] = 465; //465 //587
        $config['smtp_user'] = ''; //edit email server
        $config['smtp_pass'] = '';  //edit
        $config['smtp_crypto'] = 'security';
        $config['mailtype'] = 'html';
        $config['smtp_timeout'] = '4';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['newline'] = "\r\n"; //use double quotes
        
        $this->load->library('email', $config);
        $this->email->initialize($config);

        //send mail
        $this->email->to('muhfarhan.ibrahim@gmail.com');
        $this->email->from($email, $name);
        $this->email->subject($subjek);
        $this->email->message($message);

        if ($this->email->send()) {
            $this->session->set_flashdata('success','Message sent!');
            redirect(base_url('contactus'));
        } else {
            show_error($this->email->print_debugger());
        }
        

        
    }
}
