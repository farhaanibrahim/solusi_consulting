<?php
class Admin extends CI_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('DataModel');
    }

    public function index()
    {
        if ($this->session->userdata('status')=='login') {
            $this->load->view('backend/dashboard');
        } else {
            redirect(base_url('admin/login'));
        }
        
    }

    public function login()
    {
        if ($this->session->userdata('status')=='login') {
            redirect(base_url('admin/dashboard'));
        } else {
            $this->load->view('backend/login');
        }
        
    }

    public function loginProcess()
    {
        if ($this->session->userdata('status') == 'login') {
        redirect(base_url('admin/dashboard'));
        } else {
        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));

        $data = array(
            'username'=>$username,
            'password'=>$password
        );
        $query = $this->DataModel->login($data);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
            if ($row->username == $username && $row->password == $password) {
                $session = array(
                    'id'=>$row->id,
                    'username'=>$row->username,
                    'status'=>'login'
                );
                $this->session->set_userdata($session);
                redirect(base_url('admin/dashboard'));
            } else {
                $this->session->set_flashdata('error','Wrong username or password!');
                redirect(base_url('admin/login'));
                }
            }
        } else {
                $this->session->set_flashdata('error','Wrong username or password!');
                redirect(base_url('admin/login'));
            }
        }
    }

    public function logout()
    {
        if ($this->session->userdata('status') == 'login') {
            $this->session->sess_destroy();
            redirect(base_url('admin/login'));
          } else {
            redirect(base_url('admin/login'));
          }
    }

    public function dashboard()
    {
        if ($this->session->userdata('status')=='login') {
            $data['inbox'] = $this->DataModel->inboxDashboard();
            $this->load->view('backend/dashboard',$data);
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function inbox()
    {
        if ($this->session->userdata('status')=='login') {
            $data['inbox'] = $this->DataModel->getInbox();
            $data['count_unread'] = $this->DataModel->getUnreadMessages();
            
            $this->load->view('backend/contact',$data);
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function read($id)
    {
        if ($this->session->userdata('status')=='login') {
            $data['pesan'] = $this->DataModel->getInboxByID($id);
            
            $this->DataModel->messageToRead($id);

            $this->load->view('backend/readMail',$data);
        } else {
            redirect(base_url('admin/login'));
        }
        
    }

    public function gallery()
    {
        if ($this->session->userdata('status')=='login') {
            $data['gallery'] = $this->DataModel->getGallery();
            $this->load->view('backend/gallery',$data);
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function inputGallery()
    {
        if ($this->session->userdata('status')=='login') {
            $this->load->view('backend/inputGallery');
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function inputGalleryProccess()
    {
        if ($this->session->userdata('status')=='login') {
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10240;

            $this->load->library('upload', $config);
            
            if ( ! $this->upload->do_upload('foto'))
            {
                $this->session->set_flashdata('error',$this->upload->display_errors());

                redirect(base_url('admin/inputGallery'));
            }
            else
            {
                $judul = $this->input->post('judul');
                $deskripsi = $this->input->post('deskripsi');
                $foto = $this->upload->data('file_name');

                $data = array(
                    'id'=>'',
                    'judul'=>$judul,
                    'deskripsi'=>$deskripsi,
                    'foto'=>$foto,
                    'tgl'=>date('Ymd')
                );
                $this->DataModel->inputGallery($data);

                $this->session->set_flashdata('success','Data telah tersimpan!');
                redirect(base_url('admin/gallery'));
            }
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function galleryEdit($id)
    {
        if ($this->session->userdata('status')=='login') {
            $data['gallery'] = $this->DataModel->getGalleryByID($id);
            $this->load->view('backend/galleryEdit',$data);
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function galleryEditProccess($id)
    {
        if ($this->session->userdata('status')=='login') {
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10240;

            $this->load->library('upload', $config);
            
            if ( ! $this->upload->do_upload('foto'))
            {
                $judul = $this->input->post('judul');
                $deskripsi = $this->input->post('deskripsi');
                $foto = $this->input->post('oldPhoto');

                $data = array(
                    'judul'=>$judul,
                    'deskripsi'=>$deskripsi,
                    'foto'=>$foto
                );
                $this->DataModel->galleryUpdate($id,$data);

                $this->session->set_flashdata('success','Data berhasil diupdate!');
                redirect(base_url('admin/gallery'));
            }
            else
            {
                $judul = $this->input->post('judul');
                $deskripsi = $this->input->post('deskripsi');
                $foto = $this->upload->data('file_name');

                $data = array(
                    'judul'=>$judul,
                    'deskripsi'=>$deskripsi,
                    'foto'=>$foto
                );
                $this->DataModel->galleryUpdate($id,$data);

                unlink("uploads/".$this->input->post('oldPhoto')."");

                $this->session->set_flashdata('success','Data berhasil diupdate!');
                redirect(base_url('admin/gallery'));
            }
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function galleryDelete($id)
    {
        if ($this->session->userdata('status')=='login') {
            $getDataGallery = $this->DataModel->getGalleryByID($id);
            foreach ($getDataGallery->result() as $row) {
                $nm_foto = $row->foto;
            }

            unlink("uploads/".$nm_foto."");

            $this->DataModel->galleryDelete($id);
            $this->session->set_flashdata('success','Data telah dihapus!');

            redirect(base_url('admin/gallery'));
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function account()
    {
        if ($this->session->userdata('status')=='login') {
            $data['account'] = $this->DataModel->getAccountByID($this->session->userdata('id'));
            $this->load->view('backend/changePassword',$data);
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function changePassword()
    {
        if ($this->session->userdata('status')=='login') {
            $id = $this->input->post('id');
            $username = $this->input->post('username');
            $oldPassword = md5($this->input->post('oldPassword'));
            $newPassword = md5($this->input->post('newPassword'));
            $retypePassword = md5($this->input->post('retype_newPassword'));

            $oldAccountInfo = $this->DataModel->getAccountByID($id);
            foreach ($oldAccountInfo->result() as $row) {
                if ($row->password == $oldPassword) {
                    if ($newPassword == $retypePassword) {
                        $data = array(
                            'username'=>$username,
                            'password'=>$newPassword
                        );
                        $this->DataModel->accountUpdate($id,$data);
                        $this->session->set_userdata('username',$username);

                        $this->session->set_flashdata('success','Data berhasi diubah');
                        redirect(base_url('admin/account'));
                    } else {
                        $this->session->set_flashdata('error','re-type Password does not match with new password, please try again');
                        redirect(base_url('admin/account'));
                    }
                    
                } else {
                    $this->session->set_flashdata('error','Old Password does not match, please try again');
                    redirect(base_url('admin/account'));
                }
                
            }
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function visi()
    {
        if ($this->session->userdata('status')=='login') {
            $data['visi'] = $this->DataModel->getVisi();

            $this->load->view('backend/visi',$data);
        } else {
            redirect(base_url('admin/login'));
        }
        
    }

    public function visiEdit()
    {
        if ($this->session->userdata('status')=='login') {
            $visi = $this->input->post('visi');
            $this->DataModel->updateVisi($visi);
            $this->session->set_flashdata('success','Saved!');

            redirect(base_url('admin/visi'));
        } else {
            redirect(base_url('admin/login'));
        }
        
    }

    public function misi()
    {
        if ($this->session->userdata('status')=='login') {
            $data['misi'] = $this->DataModel->getMisi();

            $this->load->view('backend/misi',$data);
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function misiEdit()
    {
        if ($this->session->userdata('status')=='login') {
            $misi = $this->input->post('misi');
            $this->DataModel->updateMisi($misi);
            $this->session->set_flashdata('success','Saved!');

            redirect(base_url('admin/misi'));
        } else {
            redirect(base_url('admin/login'));
        }
        
    }

    public function konsultasi()
    {
        if ($this->session->userdata('status')=='login') {
            $data['consulting'] = $this->DataModel->getConsulting();
            $this->load->view('backend/konsultasi',$data);
        } else {
            redirect(base_url('admin/login'));
        }
        
    }

    public function consultingInput()
    {
        if ($this->session->userdata('status')=='login') {
            $data['jns_konsultasi'] = $this->DataModel->jns_konsultasi();
            $this->load->view('backend/consultingInput',$data);
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function consultingInputProcess()
    {
        if ($this->session->userdata('status')=='login') {
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10240;

            $this->load->library('upload', $config);
            
            if ( ! $this->upload->do_upload('foto'))
            {
                $this->session->set_flashdata('error',$this->upload->display_errors());

                redirect(base_url('admin/consultingInput'));
            }
            else
            {
                $nm_konsultasi = $this->input->post('nm_konsultasi');
                $jns_konsultasi = $this->input->post('jns_konsultasi');
                $sertifikat = $this->input->post('sertifikat');
                $detail = $this->input->post('detail');
                $foto = $this->upload->data('file_name');

                $data = array(
                    'id'=>'',
                    'nm_konsul'=>$nm_konsultasi,
                    'detail'=>$detail,
                    'jns_konsul'=>$jns_konsultasi,
                    'sertifikat'=>$sertifikat,
                    'foto'=>$foto
                );
                $this->DataModel->consultingInput($data);

                $this->session->set_flashdata('success','Data telah tersimpan!');
                redirect(base_url('admin/konsultasi'));
            }
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function consultingEdit($id)
    {
        if ($this->session->userdata('status')=='login') {
            $data['consulting'] = $this->DataModel->getConsultingByID($id);
            $data['jns_konsultasi'] = $this->DataModel->jns_konsultasi();

            $this->load->view('backend/consultingEdit',$data);
        } else {
            redirect(base_url('admin/login'));
        }
        
    }

    public function consultingEditProcess($id)
    {
        if ($this->session->userdata('status')=='login') {
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10240;

            $this->load->library('upload', $config);
            
            if ( ! $this->upload->do_upload('foto'))
            {
                $nm_konsultasi = $this->input->post('nm_konsultasi');
                $jns_konsultasi = $this->input->post('jns_konsultasi');
                $sertifikat = $this->input->post('sertifikat');
                $detail = $this->input->post('detail');

                $data = array(
                    'nm_konsul'=>$nm_konsultasi,
                    'detail'=>$detail,
                    'jns_konsul'=>$jns_konsultasi,
                    'sertifikat'=>$sertifikat
                );
                $this->DataModel->consultingUpdate($id,$data);

                $this->session->set_flashdata('success','Data telah tersimpan!');
                redirect(base_url('admin/konsultasi'));
            }
            else
            {
                $nm_konsultasi = $this->input->post('nm_konsultasi');
                $jns_konsultasi = $this->input->post('jns_konsultasi');
                $sertifikat = $this->input->post('sertifikat');
                $detail = $this->input->post('detail');
                $foto = $this->upload->data('file_name');

                $data = array(
                    'nm_konsul'=>$nm_konsultasi,
                    'detail'=>$detail,
                    'jns_konsul'=>$jns_konsultasi,
                    'sertifikat'=>$sertifikat,
                    'foto'=>$foto
                );
                $this->DataModel->consultingUpdate($id,$data);

                unlink("uploads/".$this->input->post('oldPhoto')."");

                $this->session->set_flashdata('success','Data telah tersimpan!');
                redirect(base_url('admin/konsultasi'));
            }
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function consultingDelete($id)
    {
        if ($this->session->userdata('status')=='login') {
            $dataConsul = $this->DataModel->getConsultingByID($id);
            foreach ($dataConsul->result() as $row) {
                $nm_foto = $row->foto;
            }

            unlink("uploads/".$nm_foto."");

            $this->DataModel->consultingDelete($id);

            $this->session->set_flashdata('success','Data telah dihapus!');

            redirect(base_url('admin/konsultasi'));
        } else {
            redirect(base_url('admin/login'));
        }
        
    }

    public function training()
    {
        if ($this->session->userdata('status')=='login') {
            $data['training'] = $this->DataModel->getTraining();
            $this->load->view('backend/training',$data);
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function trainingInput()
    {
        if ($this->session->userdata('status')=='login') {
            $data['jns_training'] = $this->DataModel->jns_training();
            $this->load->view('backend/trainingInput',$data);
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function trainingInputProcess()
    {
        if ($this->session->userdata('status')=='login') {
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10240;

            $this->load->library('upload', $config);
            
            if ( ! $this->upload->do_upload('foto'))
            {
                $this->session->set_flashdata('error',$this->upload->display_errors());

                redirect(base_url('admin/trainingInput'));
            }
            else
            {
                $nm_training = $this->input->post('nm_training');
                $jns_training = $this->input->post('jns_training');
                $sertifikat = $this->input->post('sertifikat');
                $detail = $this->input->post('detail');
                $foto = $this->upload->data('file_name');

                $data = array(
                    'id'=>'',
                    'nm_training'=>$nm_training,
                    'detail'=>$detail,
                    'jns_training'=>$jns_training,
                    'sertifikat'=>$sertifikat,
                    'foto'=>$foto
                );
                $this->DataModel->trainingInput($data);

                $this->session->set_flashdata('success','Data telah tersimpan!');
                redirect(base_url('admin/training'));
            }
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function trainingEdit($id)
    {
        if ($this->session->userdata('status')=='login') {
            $data['training'] = $this->DataModel->getTrainingByID($id);
            $data['jns_training'] = $this->DataModel->jns_training();
            $this->load->view('backend/trainingEdit',$data);
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function trainingEditProcess($id)
    {
        if ($this->session->userdata('status')=='login') {
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10240;

            $this->load->library('upload', $config);
            
            if ( ! $this->upload->do_upload('foto'))
            {
                $nm_training = $this->input->post('nm_training');
                $jns_training = $this->input->post('jns_training');
                $sertifikat = $this->input->post('sertifikat');
                $detail = $this->input->post('detail');

                $data = array(
                    'nm_training'=>$nm_training,
                    'detail'=>$detail,
                    'jns_training'=>$jns_training,
                    'sertifikat'=>$sertifikat
                );
                $this->DataModel->trainingUpdate($id,$data);

                $this->session->set_flashdata('success','Data telah tersimpan!');
                redirect(base_url('admin/training'));
            }
            else
            {
                $nm_training = $this->input->post('nm_training');
                $jns_training = $this->input->post('jns_training');
                $sertifikat = $this->input->post('sertifikat');
                $detail = $this->input->post('detail');
                $foto = $this->upload->data('file_name');

                $data = array(
                    'nm_training'=>$nm_training,
                    'detail'=>$detail,
                    'jns_training'=>$jns_training,
                    'sertifikat'=>$sertifikat,
                    'foto'=>$foto
                );
                $this->DataModel->trainingUpdate($id,$data);

                unlink("uploads/".$this->input->post('oldPhoto')."");

                $this->session->set_flashdata('success','Data telah tersimpan!');
                redirect(base_url('admin/training'));
            }
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function trainingDelete($id)
    {
        if ($this->session->userdata('status')=='login') {
            $dataTraining = $this->DataModel->getTrainingByID($id);
            foreach ($dataTraining->result() as $row) {
                $nm_foto = $row->foto;
            }

            unlink("uploads/".$nm_foto."");

            $this->DataModel->trainingDelete($id);

            $this->session->set_flashdata('success','Data telah dihapus!');

            redirect(base_url('admin/training'));
        } else {
            redirect(base_url('admin/login'));
        }
        
    }

    public function client()
    {
        if ($this->session->userdata('status')=='login') {
            $data['client'] = $this->DataModel->getClient();
            $this->load->view('backend/client',$data);
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function inputClient()
    {
        if ($this->session->userdata('status')=='login') {
            $this->load->view('backend/inputClient');
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function inputClientProcess()
    {
        if ($this->session->userdata('status')=='login') {
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10240;

            $this->load->library('upload', $config);
            
            if ( ! $this->upload->do_upload('foto'))
            {
                $this->session->set_flashdata('error',$this->upload->display_errors());

                redirect(base_url('admin/inputClient'));
            }
            else
            {
                $perusahaan = $this->input->post('perusahaan');
                $foto = $this->upload->data('file_name');

                $data = array(
                    'id'=>'',
                    'perusahaan'=>$perusahaan,
                    'foto'=>$foto
                );
                $this->DataModel->clientInput($data);

                $this->session->set_flashdata('success','Data telah tersimpan!');
                redirect(base_url('admin/client'));
            }
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function editClient($id)
    {
        if ($this->session->userdata('status')=='login') {
            $data['client'] = $this->DataModel->getClientByID($id);
            $this->load->view('backend/editClient',$data);
        } else {
            redirect(base_url('admin/login'));
        }
        
    }

    public function editClientProcess($id)
    {
        if ($this->session->userdata('status')=='login') {
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10240;

            $this->load->library('upload', $config);
            
            if ( ! $this->upload->do_upload('foto'))
            {
                $perusahaan = $this->input->post('perusahaan');

                $data = array(
                    'perusahaan'=>$perusahaan
                );
                $this->DataModel->updateClient($id,$data);

                $this->session->set_flashdata('success','Data telah tersimpan!');
                redirect(base_url('admin/client'));
            }
            else
            {
                $perusahaan = $this->input->post('perusahaan');
                $foto = $this->upload->data('file_name');

                $data = array(
                    'perusahaan'=>$perusahaan,
                    'foto'=>$foto
                );
                $this->DataModel->updateClient($id,$data);

                unlink("uploads/".$this->input->post('oldPhoto')."");

                $this->session->set_flashdata('success','Data telah tersimpan!');
                redirect(base_url('admin/client'));
            }
        } else {
            redirect(base_url('admin/login'));
        }
    }

    public function deleteClient($id)
    {
        if ($this->session->userdata('status')=='login') {
            $data = $this->DataModel->getClientByID($id);
            foreach ($data->result() as $row) {
                $foto = $row->foto;
            }
            unlink("uploads/".$foto."");

            $this->DataModel->deleteClient($id);

            $this->session->set_flashdata('success','Data telah dihapus!');
            redirect(base_url('admin/client'));
        } else {
            redirect(base_url('admin/login'));
        }
        
    }
}
