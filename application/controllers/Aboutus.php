<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Aboutus extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('DataModel');
    }
    public function index()
    {
        $data['visi'] = $this->DataModel->getVisi();
        $data['misi'] = $this->DataModel->getMisi();
        $data['client'] = $this->DataModel->getClient();
        $this->load->view('frontend/aboutus',$data);
    }
}
