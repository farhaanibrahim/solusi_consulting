<?php
class DataModel extends CI_Model
{
    public function login($data)
    {
        $this->db->where($data);
        return $this->db->get('user');
    }

    public function getAccountByID($id)
    {
        $this->db->where('id',$id);
        return $this->db->get('user');
    }

    public function accountUpdate($id,$data)
    {
        $this->db->where('id',$id);
        $this->db->set($data);
        $this->db->update('user');
    }

    public function getInbox()
    {
        $this->db->order_by('id','desc');
        return $this->db->get('contact');
    }

    public function inboxDashboard()
    {
        $this->db->order_by('id','desc');
        $this->db->limit(10);
        return $this->db->get('contact');
    }

    public function getUnreadMessages()
    {
        $this->db->where('status','unread');
        return $this->db->get('contact');
    }

    public function getInboxByID($id)
    {
        $this->db->where('id',$id);
        return $this->db->get('contact');
    }

    public function messageToRead($id)
    {
        $this->db->where('id',$id);
        $this->db->set('status','read');
        $this->db->update('contact');
    }

    public function unreadMessage()
    {
        $this->db->where('status','unread');
        return $this->db->get('contact');
    }
    
    public function getGallery()
    {
        return $this->db->get('gallery');
    }

    public function getGalleryByID($id)
    {
        $this->db->where('id',$id);
        return $this->db->get('gallery');
    }

    public function inputGallery($data)
    {
        $this->db->insert('gallery',$data);
    }

    public function galleryUpdate($id,$data)
    {
        $this->db->where('id',$id);
        $this->db->set($data);
        $this->db->update('gallery');
    }

    public function galleryDelete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('gallery');
    }

    public function getVisi()
    {
        $this->db->where('id',1);
        return $this->db->get('visi');
    }

    public function getMisi()
    {
        $this->db->where('id',1);
        return $this->db->get('misi');
    }

    public function updateVisi($visi)
    {
        $this->db->where('id',1);
        $this->db->set('konten_visi',$visi);
        $this->db->update('visi');
    }

    public function updateMisi($misi)
    {
        $this->db->where('id',1);
        $this->db->set('konten_misi',$misi);
        $this->db->update('misi');
    }

    public function jns_konsultasi()
    {
        return $this->db->get('kategori_konsul');
    }

    public function getConsulting()
    {
        $this->db->join('kategori_konsul','kategori_konsul.id_konsul = konsultasi.jns_konsul');
        return $this->db->get('konsultasi');
    }

    public function getConsultingForHomepage()
    {
        $this->db->join('kategori_konsul','kategori_konsul.id_konsul = konsultasi.jns_konsul');
        $this->db->limit(4);
        return $this->db->get('konsultasi');
    }

    public function consultingInput($data)
    {
        $this->db->insert('konsultasi',$data);
    }

    public function getConsultingByID($id)
    {
        $this->db->where('konsultasi.id',$id);
        $this->db->join('kategori_konsul','kategori_konsul.id_konsul = konsultasi.jns_konsul');
        return $this->db->get('konsultasi');
    }

    public function consultingUpdate($id,$data)
    {
        $this->db->where('id',$id);
        $this->db->set($data);
        $this->db->update('konsultasi');
    }

    public function consultingDelete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('konsultasi');
    }

    public function getTraining()
    {
        $this->db->join('kategori_training','kategori_training.id_training = training.jns_training');
        return $this->db->get('training');
    }

    public function getTrainingForHomepage()
    {
        $this->db->join('kategori_training','kategori_training.id_training = training.jns_training');
        $this->db->limit(4);
        return $this->db->get('training');
    }

    public function jns_training()
    {
        return $this->db->get('kategori_training');
    }

    public function trainingInput($data)
    {
        $this->db->insert('training',$data);
    }

    public function getTrainingByID($id)
    {
        $this->db->where('training.id',$id);
        $this->db->join('kategori_training','kategori_training.id_training = training.jns_training');
        return $this->db->get('training');
    }

    public function trainingUpdate($id,$data)
    {
        $this->db->where('id',$id);
        $this->db->set($data);
        $this->db->update('training');
    }

    public function trainingDelete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('training');
    }

    public function sendMessage($data)
    {
        $this->db->insert('contact',$data);
    }

    public function getClient()
    {
        return $this->db->get('client');
    }

    public function clientInput($data)
    {
        $this->db->insert('client',$data);
    }

    public function getClientByID($id)
    {
        $this->db->where('id',$id);
        return $this->db->get('client');
    }

    public function updateClient($id,$data)
    {
        $this->db->where('id',$id);
        $this->db->set($data);
        $this->db->update('client');
    }

    public function deleteClient($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('client');
    }
}
