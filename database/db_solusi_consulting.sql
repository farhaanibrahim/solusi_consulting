-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 07, 2018 at 10:05 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.1.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_solusi_consulting`
--

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(15) NOT NULL,
  `perusahaan` varchar(30) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `perusahaan`, `foto`) VALUES
(1, 'ANUGRAH', 'ANUGRAH.png'),
(2, 'ASAHI', 'ASAHI.png'),
(3, 'Bumi Mulia', 'BUMIMULIA.png'),
(4, 'B4T', 'B4T.png'),
(5, 'BUMI SIAK', 'BUMI_SIAK.png'),
(6, 'DANRILIS', 'DANRILIS.png'),
(8, 'DATAPALMA', 'DUTAPALMA.png'),
(9, 'IAIN AMBON', 'IAIN_AMBON.png'),
(10, 'INTI STANIA', 'INTI_STANIA.png'),
(11, 'KAHATEX', 'KAHATEX.png'),
(12, 'KEMIKO', 'KEMIKO.png'),
(13, 'KINO', 'KINO.png'),
(14, 'LOTTE PACKAGING', 'LOTTE_PACKAGING.png'),
(15, 'LPMP', 'LPMP.png'),
(16, 'MANDOM', 'MANDOM.png'),
(17, 'MITSUBISHI', 'MITSUBISHI_LOGISTICS.png');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(20) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `subjek` varchar(50) NOT NULL,
  `pesan` text NOT NULL,
  `tgl` date NOT NULL,
  `status` enum('unread','read') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `nama`, `email`, `subjek`, `pesan`, `tgl`, `status`) VALUES
(1, 'Muhammad Farhan', 'farhaaanibrahim@gmail.com', '-', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2018-06-12', 'unread'),
(2, 'Feby Melati', 'febymmaarani@gmail.com', '-', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2018-06-12', 'unread'),
(3, 'Akbar Wibawanti', 'akbarw@gmail.com', 'Lorem Ipsum', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2018-06-12', 'unread'),
(4, 'Bagas Nur A', 'bagasna@gmail.com', 'Lorem Ipsum', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2018-06-12', 'unread'),
(5, 'Rinaldo Augrah W', 'rinaldoaw@gmail.com', '-', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2018-06-12', 'read'),
(8, 'Muhammad Farhan Ibrahim', 'muhfarhan.ibrahim@gmail.com', 'Test', 'Message From : Muhammad Farhan Ibrahim<br>Email : muhfarhan.ibrahim@gmail.com<br>Subjek : Test<br>Message : <br>Test', '2018-06-20', 'unread'),
(9, 'Muhammad Farhan Ibrahim', 'farhaaanibrahim@gmail.com', 'Testing', 'Message From : Muhammad Farhan Ibrahim<br>Email : farhaaanibrahim@gmail.com<br>Subjek : Testing<br>Message : <br>Testing', '2018-06-20', 'read');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(20) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `foto` varchar(100) NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `judul`, `deskripsi`, `foto`, `tgl`) VALUES
(1, 'TRAINING DI PT. MANDOM INDONESIA, TBK.', '                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Turpis massa sed elementum tempus egestas sed. Lacinia at quis risus sed vulputate odio ut enim. Sagittis orci a scelerisque purus semper eget duis. Eget nulla facilisi etiam dignissim diam quis enim lobortis scelerisque.                ', '1.jpg', '2018-06-13'),
(2, 'TRAINING DI PT. MANDOM INDONESIA, TBK.', '                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque adipiscing commodo elit at imperdiet dui accumsan. Mauris a diam maecenas sed enim ut sem viverra. Tristique et egestas quis ipsum suspendisse. Neque ornare aenean euismod elementum nisi quis eleifend quam adipiscing.                ', '4.jpg', '2018-06-13'),
(3, 'TRAINING DI PT. MANDOM INDONESIA, TBK.', '                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Diam sit amet nisl suscipit adipiscing bibendum est ultricies integer. Faucibus turpis in eu mi bibendum. Nibh tortor id aliquet lectus. Luctus accumsan tortor posuere ac ut consequat.                ', '5.jpg', '2018-06-13'),
(4, 'vehicula ipsum a arcu cursus', '                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dignissim cras tincidunt lobortis feugiat. Eget duis at tellus at urna condimentum mattis. Sed libero enim sed faucibus turpis in eu mi. Aliquam etiam erat velit scelerisque.                                                ', 'konsultan-keuangan-huffpost.com_.jpg', '2018-06-13'),
(6, 'tincidunt vitae semper quis lectus', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Commodo nulla facilisi nullam vehicula ipsum a arcu. Adipiscing elit duis tristique sollicitudin nibh sit. Aliquam id diam maecenas ultricies mi eget mauris. Nec dui nunc mattis enim ut tellus elementum.', 'konsultan23adawd.jpg', '2018-06-13'),
(7, 'lectus proin nibh nisl condimentum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Turpis nunc eget lorem dolor sed viverra ipsum nunc. Quis ipsum suspendisse ultrices gravida dictum fusce ut placerat orci. Habitant morbi tristique senectus et. Enim ut tellus elementum sagittis vitae et.', 'consulting-homepage-2.jpg', '2018-06-30');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_konsul`
--

CREATE TABLE `kategori_konsul` (
  `id_konsul` int(20) NOT NULL,
  `jns_konsul` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_konsul`
--

INSERT INTO `kategori_konsul` (`id_konsul`, `jns_konsul`) VALUES
(1, 'Quality Management'),
(2, 'Environment Energy'),
(3, 'Health Safety'),
(4, 'Food Safety'),
(5, 'Information Security'),
(6, 'Risk & Management'),
(7, 'Konsultasi Lain-Lain');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_training`
--

CREATE TABLE `kategori_training` (
  `id_training` int(20) NOT NULL,
  `jns_training` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_training`
--

INSERT INTO `kategori_training` (`id_training`, `jns_training`) VALUES
(1, 'In - House'),
(2, 'Public');

-- --------------------------------------------------------

--
-- Table structure for table `konsultasi`
--

CREATE TABLE `konsultasi` (
  `id` int(20) NOT NULL,
  `nm_konsul` varchar(50) NOT NULL,
  `detail` text NOT NULL,
  `jns_konsul` varchar(20) NOT NULL,
  `sertifikat` varchar(50) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konsultasi`
--

INSERT INTO `konsultasi` (`id`, `nm_konsul`, `detail`, `jns_konsul`, `sertifikat`, `foto`) VALUES
(1, 'mauris cursus mattis molestie', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mollis nunc sed id semper risus. Commodo odio aenean sed adipiscing diam. Rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi. Tincidunt arcu non sodales neque sodales ut etiam sit.', '1', 'ISO 9001:2015', 'Jasa-Konsultasi-Titan.jpg'),
(2, 'iaculis urna id volutpat lacus', '                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Gravida arcu ac tortor dignissim convallis aenean et tortor at. Faucibus ornare suspendisse sed nisi lacus sed viverra tellus. Laoreet id donec ultrices tincidunt. In cursus turpis massa tincidunt dui.                    ', '1', 'ISO 9001:2015', 'wallpapersden_com_forest-mountains-sunset-cool-weather-minimalism_3840x2160.jpg'),
(3, 'arcu non odio euismod lacinia', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porta non pulvinar neque laoreet suspendisse interdum consectetur. Odio euismod lacinia at quis risus sed vulputate. Risus sed vulputate odio ut enim blandit volutpat maecenas. Urna condimentum mattis pellentesque id nibh tortor.', '1', 'ISO 9001:2015', 'konsultasi-motivasi.jpg'),
(4, 'tellus orci ac auctor augue', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Porta non pulvinar neque laoreet suspendisse interdum consectetur. Odio euismod lacinia at quis risus sed vulputate. Risus sed vulputate odio ut enim blandit volutpat maecenas. Urna condimentum mattis pellentesque id nibh tortor.', '2', 'ISO 9001:2015', 'oil-gas.jpg'),
(5, '', '                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis posuere morbi leo urna. Tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras. Massa tincidunt nunc pulvinar sapien et. Quisque non tellus orci ac auctor augue mauris augue.\r\n                                        ', '', 'ISO 9001:2015', 'Environment-energy-transition-renewable-energie-AEE.jpg'),
(6, 'vitae nunc sed velit dignissim', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ante in nibh mauris cursus mattis molestie. Platea dictumst vestibulum rhoncus est pellentesque. Pulvinar mattis nunc sed blandit libero. At urna condimentum mattis pellentesque id nibh tortor id.', '6', 'ISO 9001:2015', 'Banking-careers-in-south-africa.jpg'),
(8, 'et magnis dis parturient montes', '                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nec sagittis aliquam malesuada bibendum arcu vitae elementum curabitur vitae. Sit amet porttitor eget dolor. Ut venenatis tellus in metus vulputate eu scelerisque felis imperdiet. Velit scelerisque in dictum non consectetur a erat nam at.                                        ', '5', 'ISO 9001:2015', 'security.jpg'),
(9, 'Kondultsdi B', 'Lorem Ipsum', '1', '9002', '3865967-wallpaper-full-hd_XNgM7er.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `misi`
--

CREATE TABLE `misi` (
  `id` int(20) NOT NULL,
  `konten_misi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `misi`
--

INSERT INTO `misi` (`id`, `konten_misi`) VALUES
(1, '<p>Berkomitmen untuk memberikan pelayanan yang berkualitas untuk memuaskan mitra<br />\r\nusaha melalui profesionalisme dan jaringan yang luas</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `training`
--

CREATE TABLE `training` (
  `id` int(20) NOT NULL,
  `nm_training` varchar(50) NOT NULL,
  `detail` text NOT NULL,
  `jns_training` varchar(20) NOT NULL,
  `sertifikat` varchar(50) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `training`
--

INSERT INTO `training` (`id`, `nm_training`, `detail`, `jns_training`, `sertifikat`, `foto`) VALUES
(1, 'pellentesque elit ullamcorper dignissim cras', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet mauris commodo quis. Ornare arcu odio ut sem nulla pharetra diam sit amet. Tempus egestas sed sed risus pretium quam vulputate. Amet justo donec enim diam vulputate ut pharetra.', '1', 'ISO 9001:2015', 'in-house-training.jpg'),
(2, 'maecenas sed enim ut sem', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Tellus integer feugiat scelerisque varius morbi enim nunc faucibus. Vestibulum sed arcu non odio euismod. Molestie a iaculis at erat pellentesque adipiscing commodo elit. Vestibulum morbi blandit cursus risus at ultrices mi tempus.', '1', 'ISO 9001:2015', 'in-house-training__123.jpg'),
(3, 'vitae semper quis lectus nulla', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nunc sed blandit libero volutpat sed. Mattis vulputate enim nulla aliquet. Turpis nunc eget lorem dolor. Sollicitudin ac orci phasellus egestas.', '2', 'ISO 9001:2015', '371727-636456691296187188-16x9.jpg'),
(4, 'nibh nisl condimentum id venenatis', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Et odio pellentesque diam volutpat commodo sed. Eu lobortis elementum nibh tellus. Auctor eu augue ut lectus arcu bibendum at. Arcu dui vivamus arcu felis bibendum ut tristique et.', '2', 'ISO 9001:2015', 'Public-Training.png'),
(6, 'placerat vestibulum lectus mauris ultrices', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Convallis posuere morbi leo urna. Tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras. Massa tincidunt nunc pulvinar sapien et. Quisque non tellus orci ac auctor augue mauris augue.', '2', 'ISO 9001:2015', 'public-classes-icon-335x335.png');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(20) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `visi`
--

CREATE TABLE `visi` (
  `id` int(20) NOT NULL,
  `konten_visi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visi`
--

INSERT INTO `visi` (`id`, `konten_visi`) VALUES
(1, '<p>Menjadi perusahaan yang handal, terpercaya dan memiliki reputasi luar biasa secara<br />\r\nGlobal</p>\r\n');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_konsul`
--
ALTER TABLE `kategori_konsul`
  ADD PRIMARY KEY (`id_konsul`);

--
-- Indexes for table `kategori_training`
--
ALTER TABLE `kategori_training`
  ADD PRIMARY KEY (`id_training`);

--
-- Indexes for table `konsultasi`
--
ALTER TABLE `konsultasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `misi`
--
ALTER TABLE `misi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `training`
--
ALTER TABLE `training`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visi`
--
ALTER TABLE `visi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `kategori_konsul`
--
ALTER TABLE `kategori_konsul`
  MODIFY `id_konsul` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `kategori_training`
--
ALTER TABLE `kategori_training`
  MODIFY `id_training` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `konsultasi`
--
ALTER TABLE `konsultasi`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `misi`
--
ALTER TABLE `misi`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `training`
--
ALTER TABLE `training`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `visi`
--
ALTER TABLE `visi`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
